import qualified Data.Array as A
import qualified Data.ByteString as B
import qualified Data.ByteString.Internal as B (c2w, w2c)
import qualified Data.ByteString.Char8 as C
import qualified Data.Map.Strict as Map
import qualified Data.IntSet as Set
import Control.Parallel.Strategies
import Data.Array (Array)
import Data.ByteString (ByteString)
import Data.ByteString.Char8 ()
import Data.Map.Strict (Map)
import Data.Maybe
import Data.IntSet (IntSet)

main :: IO ()
main = do
    input <- B.getContents
    let (firstRow:rows) = C.lines input
        n = (read . (map B.w2c) $ B.unpack . head . C.words $ firstRow) :: Int
        ws = take n rows
        qs = map C.words $ drop n rows
        wordLookup = Map.fromList . zip ws $ [0..n-1]
        wsArr = A.listArray (0,n-1) ws
        graph = makeWordMap wsArr [0..n-1] ws
        formatPath path = if path == -1 then "Impossible" else show path
        paths = unlines $ parMap rdeepseq (\(u:v:_)->formatPath $ shortestPath graph wordLookup u v) qs 
    putStr $ paths

findAndErase :: Char -> ByteString -> ByteString -> Maybe ByteString
findAndErase x searched str
    | B.null str = Nothing
    | (B.c2w x) == c = Just (B.append searched str')
    | otherwise = findAndErase x (c `B.cons` searched) str'
    where (c, str') = fromMaybe (error "Failed findAndErase") . B.uncons $ str

isEdge :: ByteString -> ByteString -> Bool
isEdge u v
    | B.null v' = True
    | u == v = False
    | len == 4 = isEdge u' v
    | otherwise = case findAndErase (B.w2c c) B.empty v of 
        Just rest -> isEdge u' rest
        Nothing -> False
    where (c, u') = fromMaybe (error "Failed isEdge") . B.uncons $ u
          len = B.length u'
          v' = B.tail v

createMapEntry :: Array Int ByteString -> [Int] -> ByteString -> [Int]
createMapEntry ws is u = filter (isEdge u . (ws A.!)) is

makeWordMap :: Array Int ByteString -> [Int] -> [ByteString] -> Array Int [Int]
makeWordMap wsArr is ws = A.listArray (0,n) $ map (createMapEntry wsArr is) ws
    where n = succ . snd . A.bounds $ wsArr

bfs :: Array Int [Int] -> [Int] -> [Int] -> [Int] -> IntSet -> Int -> Int -> Int
bfs graph q qNew toVisit visited u lvl
    | q_e && qNew_e && toVisit_e = -1
    | q_e && toVisit_e = bfs graph qNew [] [] visited u (lvl+1)
    | toVisit_e = bfs graph q' qNew toVisitNew visited u lvl
    | w == u = lvl+1
    | Set.notMember w visited = bfs graph q qNew' toVisit' visited' u lvl
    | otherwise = bfs graph q qNew toVisit' visited u lvl
    where q_e = null q
          qNew_e = null qNew
          toVisit_e = null toVisit
          (v:q') = q
          toVisitNew = graph A.! v
          (w:toVisit') = toVisit
          qNew' = w:qNew
          visited' = Set.insert w visited

shortestPath :: Array Int [Int] -> Map ByteString Int -> ByteString -> ByteString -> Int
shortestPath graph wordLookup uStr vStr
    | uStr == vStr = 0
    | otherwise = bfs graph q [] toVisit visited v 0
    where str2int str = fromMaybe (error "Failed shortestPath") $ Map.lookup str wordLookup
          u = str2int uStr
          v = str2int vStr
          q = [u]
          visited = Set.fromList [u]
          toVisit = graph A.! u

