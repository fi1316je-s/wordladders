import qualified Data.Map.Strict as Map
import qualified Data.Set as Set
import Data.Map.Strict (Map)
import Data.Set (Set)
import Data.Maybe

main :: IO ()
main = do
    input <- getContents
    let (firstRow:rows) = lines input
        n = (read . head . words $ firstRow) :: Int
        ws = take n rows
        qs = map words $ drop n rows
        graph = makeWordMap ws
        formatPath path = if path == -1 then "Impossible" else show path
        paths = unlines $ map (\(u:v:_)->formatPath $ shortestPath graph u v) qs 
    putStr $ paths

findAndErase :: Char -> String -> String -> Maybe String
findAndErase _ _ [] = Nothing
findAndErase x searched (c:str)
    | x == c = Just (searched++str)
    | otherwise = findAndErase x (c:searched) str

isEdge :: String -> String -> Bool
isEdge _ (_:[]) = True
isEdge (c:u) v
    | c:u == v = False
    | len == 4 = isEdge u v
    | otherwise = case findAndErase c [] v of 
        Just rest -> isEdge u rest
        Nothing -> False
    where len = length u
isEdge _ _ = error "This shouldn't happen"

createMapEntry :: [String] -> String -> (String,  [String])
createMapEntry ws u = (u, filter (isEdge u) ws)

makeWordMap :: [String] -> Map String [String]
makeWordMap ws = Map.fromList $ map (createMapEntry ws) ws

bfs :: Map String [String] -> [String] -> [String] -> [String] -> Set String -> String -> Int -> Int
bfs graph q qNew toVisit visited u lvl
    | q_e && qNew_e && toVisit_e = -1
    | q_e && toVisit_e = bfs graph qNew [] [] visited u (lvl+1)
    | toVisit_e = bfs graph q' qNew toVisitNew visited u lvl
    | w == u = lvl+1
    | Set.notMember w visited = bfs graph q qNew' toVisit' visited' u lvl
    | otherwise = bfs graph q qNew toVisit' visited u lvl
    where q_e = null q
          qNew_e = null qNew
          toVisit_e = null toVisit
          (v:q') = q
          toVisitNew = fromMaybe (error "Lookup failed") $ Map.lookup v graph
          (w:toVisit') = toVisit
          qNew' = w:qNew
          visited' = Set.insert w visited

shortestPath :: Map String [String] -> String -> String -> Int
shortestPath graph u v
    | u == v = 0
    | otherwise = bfs graph q [] toVisit visited v 0
    where q = [u]
          visited = Set.fromList [u]
          toVisit = fromMaybe (error "Lookup failed") $ Map.lookup u graph
