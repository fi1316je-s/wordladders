function find(x, ys)
    for (i,y) in enumerate(ys)
        x === y && return i
    end
    return 0
end

function isedge(u::String, v::String)
    u === v && return false
    vs = Array{Char,1}(v)
    for c in Array{Char,1}(u[2:end])
        match = find(c, vs)
        match == 0 && return false
        vs[match] = vs[end]
        pop!(vs)
    end
    true
end

function makewordmap(wordlookup)
    wordmap = Array{Array{UInt16,1},1}(undef,length(wordlookup))
    @sync for (u,n) in wordlookup
        Threads.@spawn begin 
            neighbors = UInt16[]
            for (v,m) in wordlookup
                if isedge(u,v)
                    push!(neighbors, m)
                end
                wordmap[n] = neighbors
            end
        end
    end
    wordmap
end

function bfs(graph::Array{Array{UInt16,1},1}, q::Array{UInt16}, 
        visited::BitArray, to::UInt16, level::Int)
    isempty(q) && return -1
    q_new = UInt16[]
    for v in q
        neighbors = graph[v]
        for w in neighbors
            if visited[w]
                visited[w] = true
                push!(q_new, w)
                if w == to
                    return level+1
                end
            end
        end
    end
    bfs(graph, q_new, visited, to, level+1)
end

function bfs(graph::Array{Array{UInt16,1},1}, u::UInt16, v::UInt16)
    u === v && return 0
    visited = BitArray(repeat([false],length(graph)))
    q = [u]
    bfs(graph, q, visited, v, 0)
end

function wordladders(input)
    lines = readlines(input)
    nums = map(x->parse(Int,x),split(lines[1], " ", keepempty=false))
    N = nums[1]
    Q = nums[2]

    wordlookup = Dict{String,UInt16}()
    words = @view lines[2:N+1]
    for i=1:length(words)
        push!(wordlookup,words[i]=>i)
    end
    @time wm = makewordmap(wordlookup)
    queries = @view lines[N+2:end]
    tasklist = Array{Task,1}(undef,Q)
    for i=1:Q
        qwords = string.(split(queries[i]," ",limit=2))
        u = wordlookup[qwords[1]]
        v = wordlookup[qwords[2]]
        tasklist[i] = Threads.@spawn begin
            bfs(wm,u,v)
        end
    end
    @time map(tasklist) do task
        len = fetch(task)
        len === -1 ? "Impossible\n" : string(len,"\n")
    end |> join |> print
end


main() = wordladders(Base.stdin)

@time main()
