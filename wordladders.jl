
function charoccurance(w::String)
    chars = Dict{Char,Int}()
    for c in collect(w)
        n = get(chars, c, 0)
        chars[c] = n+1
    end
    chars
end

function isedge(u::String, v::String)
    u === v && return false
    chars = charoccurance(v)
    for c in @view collect(u)[2:end]
        n = get(chars, c, 0)
        (chars[c] = n-1) < 0 && return false
    end
    true
end

function makeworddict(words)
    worddict = Dict{String,Set{String}}()
    for i=1:length(words)
        push!(worddict,words[i]=>Set{String}())
    end
    @sync for (k,v) in worddict
        Threads.@spawn for w in words
            if isedge(k,w)
                push!(v, w)
            end
        end
    end
    worddict
end

function bfs(graph::Dict, visited::Set, tovisit::Set, w2::String, level::Int)
    isempty(tovisit) && return -1
    nexttovisit = Set{String}()
    for w in tovisit
        union!(nexttovisit, graph[w])
    end
    setdiff!(nexttovisit, visited)
    w2 in nexttovisit && return level+1
    union!(visited, tovisit)
    bfs(graph,visited,nexttovisit,w2,level+1)
end

function bfs(graph::Dict, w1::String, w2::String)
    w1 === w2 && return 0
    w2 in graph[w1] && return 1
    bfs(graph,Set{String}(),graph[w1],w2,1)
end

function wordladders(input)
    lines = readlines(input)
    nums = map(x->parse(Int,x),split(lines[1], " ", keepempty=false))
    N = nums[1]
    Q = nums[2]
    wd = makeworddict(@view lines[2:N+1])
    queries = @view lines[N+2:end]
    tasklist = Array{Task,1}(undef,Q)
    for i=1:Q
        tasklist[i] = Threads.@spawn begin
            qwords = string.(split(queries[i]," ",limit=2))
            bfs(wd,qwords[1],qwords[2])
        end
    end
    map(tasklist) do task
        len = fetch(task)
        len === -1 ? "Impossible\n" : string(len,"\n")
    end |> join |> print
end


main() = wordladders(Base.stdin)

main()