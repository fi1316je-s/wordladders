use std::collections::{HashMap};
use std::io;
use std::io::BufRead;
use std::sync::Arc;
use std::thread::{self, JoinHandle};

fn find(v: &[u8], c: &u8) -> Option<usize> {
    for (i, v) in v.iter().enumerate() {
        if *v == *c {
            return Some(i);
        }
    }
    None
}

fn is_edge(u: &str, v: &str) -> bool {
    if u == v {
        return false;
    }
    let mut v = Vec::from(v.as_bytes());
    for c in u[1..].as_bytes() {
        match find(&v, c) {
            Some(i) => v.swap_remove(i),
            None => return false,
        };
    }
    return true;
}

fn make_word_map(
    word_lookup: HashMap<String, u16>,
) -> Vec<Vec<u16>> {
    let len = word_lookup.len();
    let mut word_map: Vec<Vec<u16>> = Vec::new();
    word_map.resize(word_lookup.len(), Vec::new());
    let mut handle_list: HashMap<u16,JoinHandle<Vec<u16>>> = HashMap::with_capacity(len);
    let word_iter = word_lookup.clone();
    let word_lookup = Arc::new(word_lookup);
    for (u, n) in word_iter {
        let word_lookup = Arc::clone(&word_lookup); 
        let h = thread::spawn(move || {
            let mut neighbors: Vec<u16> = Vec::new();
            for (v, m) in word_lookup.iter() {
                if is_edge(&u, v) {
                    neighbors.push(*m);
                }
            }
            neighbors
        });
        handle_list.insert(n, h);
    }
    for (i,h) in handle_list.into_iter() {
        word_map[i as usize] = h.join().unwrap();
    }
    word_map
}

fn bfs(
    word_map: &Vec<Vec<u16>>,
    q: Vec<u16>,
    visited: Vec<bool>,
    to: u16,
    level: isize,
) -> isize {
    let mut visited = visited;
    let q = q;
    if q.is_empty() {
        return -1;
    }
    let mut q_new: Vec<u16> = Vec::new();
    for v in q {
        let neighbors = &word_map[v as usize];
        for w in neighbors {
            if !visited[*w as usize] {
                visited[*w as usize] = true;
                q_new.push(*w);
                if *w == to {
                    return level + 1;
                }
            }
        }
    }
    bfs(word_map, q_new, visited, to, level + 1)
}
fn shortest_path(word_map: &Vec<Vec<u16>>, u: u16, v: u16) -> isize {
    if u == v {
        return 0;
    }
    let mut visited: Vec<bool> = Vec::new();
    visited.resize(word_map.len(), false);
    visited[u as usize] = true;
    let q: Vec<u16> = Vec::from(vec![u]);
    return bfs(word_map, q, visited, v, 0);
}

fn word_ladders() {
    let stdin = io::stdin();
    let mut handle = stdin.lock();
    let mut first_line = String::new();
    handle
        .read_line(&mut first_line)
        .expect("Couldn't read first line");
    let nums: Vec<usize> = first_line
        .split_ascii_whitespace()
        .map(|x| x.parse().unwrap())
        .collect();
    let n = nums[0];
    let q = nums[1];
    let mut words: Vec<String> = Vec::with_capacity(n);
    let mut queries: Vec<String> = Vec::with_capacity(q);
    let mut word_lookup: HashMap<String, u16> = HashMap::with_capacity(words.len());
    for (i, line) in handle.lines().enumerate() {
        if i < n {
            let w = line.unwrap();
            words.push(w.clone());
            word_lookup.insert(w, i as u16);
        } else {
            queries.push(line.unwrap());
        }
    }
    let word_map = Arc::new(make_word_map(word_lookup.clone()));
    let mut path_lengths: Vec<JoinHandle<isize>> = Vec::with_capacity(q);
    for query in queries {
        let mut qwords = query.split_ascii_whitespace();
        let u = word_lookup[&qwords.next().unwrap().to_string()];
        let v = word_lookup[&qwords.next().unwrap().to_string()];
        let word_map = Arc::clone(&word_map);
        path_lengths.push(thread::spawn(move || shortest_path(&word_map, u, v)));
    }
    for h in path_lengths {
        let len = h.join().unwrap();
        if len == -1 {
            println!("Impossible");
        } else {
            println!("{}", len);
        }
    }
}

fn main() {
    word_ladders();
}
