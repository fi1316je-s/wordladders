use elapsed::measure_time;
use std::cell::*;
use std::collections::{HashMap, HashSet, VecDeque};
use std::io::BufRead;
use std::io::{self, Read};
use std::iter::FromIterator;
use std::rc::Rc;
use std::sync::Arc;
use std::thread::{self, JoinHandle};

struct Node<'a> {
    visited: Cell<bool>,
    neighbors: Vec<&'a String>,
    pred: Cell<String>,
}

fn find(v: &[u8], c: &u8) -> Option<usize> {
    for (i, v) in v.iter().enumerate() {
        if *v == *c {
            return Some(i);
        }
    }
    None
}

fn is_edge(u: &str, v: &str) -> bool {
    if u == v {
        return false;
    }
    let mut v = Vec::from(v.as_bytes());
    for c in u[1..].as_bytes() {
        match find(&v, c) {
            Some(i) => v.swap_remove(i),
            None => return false,
        };
    }
    return true;
}

fn make_word_map<'a>(
    word_map: &'a mut HashMap<String, Node<'a>>,
    words: &'a Vec<String>,
) -> &'a mut HashMap<String, Node<'a>> {
    for (u, mut n) in word_map.iter_mut() {
        let mut neighbors: Vec<&'a String> = Vec::new();
        for v in words.iter() {
            if is_edge(u, v) {
                neighbors.push(v);
            }
        }
        n.neighbors = neighbors;
    }
    word_map
}

fn bfs<'a>(word_map: &mut HashMap<String, Node>, from: String, to: String) -> isize {
    if from == to {
        return 0;
    }
    for n in word_map.values_mut() {
        n.visited.set(false);
    }
    let s = &word_map[&from];
    s.visited.set(true);
    let mut q: VecDeque<&String> = VecDeque::from(vec![&from]);
    while !q.is_empty() {
        let v = q.pop_front().unwrap();
        let neighbors = &word_map[v].neighbors;
        for w in neighbors {
            let w_node = &word_map[*w];
            if !w_node.visited.get() {
                w_node.visited.set(true);
                q.push_back(w);
                w_node.pred.set(v.clone());
                if **w == to {
                    return shortest_path(word_map, &from, v.clone());
                }
            }
        }
    }
    -1
}
fn shortest_path(word_map: &HashMap<String, Node>, u: &str, v: String) -> isize {
    let mut path_length = 1;
    let mut pred = v;
    loop {
        if pred == u {
            return path_length;
        }
        path_length += 1;
        pred = word_map[&pred].pred.take();
    }
}

fn word_ladders() {
    let stdin = io::stdin();
    let mut handle = stdin.lock();
    let mut first_line = String::new();
    handle
        .read_line(&mut first_line)
        .expect("Couldn't read first line");
    let nums: Vec<usize> = first_line
        .split_ascii_whitespace()
        .map(|x| x.parse().unwrap())
        .collect();
    let n = nums[0];
    let q = nums[1];
    let mut words: Vec<String> = Vec::with_capacity(n);
    let mut queries: Vec<String> = Vec::with_capacity(q);
    let mut word_map: HashMap<String, Node> = HashMap::with_capacity(words.len());
    for (i, line) in handle.lines().enumerate() {
        if i < n {
            let w = line.unwrap();
            words.push(w.clone());
            word_map.insert(
                w,
                Node {
                    visited: Cell::new(false),
                    neighbors: Vec::new(),
                    pred: Cell::new(words.last().unwrap().clone()),
                },
            );
        } else {
            queries.push(line.unwrap());
        }
    }
    let mut word_map = make_word_map(&mut word_map, &words);
    let mut path_lengths: Vec<isize> = Vec::with_capacity(q);
    for query in queries {
        let mut qwords = query.split_ascii_whitespace();
        path_lengths.push(bfs(
            &mut word_map,
            qwords.next().unwrap().to_string(),
            qwords.next().unwrap().to_string(),
        ));
    }
    for len in path_lengths {
        if len == -1 {
            println!("Impossible");
        } else {
            println!("{}", len);
        }
    }
}

fn main() {
    // let (elapsed,_) = measure_time(|| {
    //     word_ladders();
    // });
    // println!("{}", elapsed);
    word_ladders();
}
